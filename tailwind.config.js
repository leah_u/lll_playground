
// See the Tailwind configuration guide for advanced usage
// https://tailwindcss.com/docs/configuration

let plugin = require('tailwindcss/plugin')

const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  content: ['./src/**/*.{html,gleam}'],
  theme: {
    fontFamily: {
      'mono': ['Roboto Mono', ...defaultTheme.fontFamily.mono],
      'sans': ['Roboto', ...defaultTheme.fontFamily.sans],
    },
    screens: {
      'sm': '580px',
    },
  },
  plugins: [require('@tailwindcss/forms')],
}
