import bindings
import examples
import gleam/bool
import gleam/int
import gleam/javascript/array
import gleam/list
import gleam/option.{type Option, None, Some}
import gleam/string
import icons
import lustre
import lustre/attribute
import lustre/effect
import lustre/element
import lustre/element/html
import lustre/event

pub fn main() {
  let app = lustre.application(init, update, view)
  let assert Ok(_) = lustre.start(app, "#app", Nil)

  Nil
}

pub type Popup {
  Examples
  About
}

pub type OutputPanelState {
  Closed
  Minimized
  Maximized
}

pub type Model {
  Model(
    source: String,
    output: String,
    errors: Option(List(String)),
    worker: Option(bindings.WebWorker),
    output_panel: OutputPanelState,
    popup: Option(Popup),
  )
}

fn init(_flags) -> #(Model, effect.Effect(Msg)) {
  let code = case examples.examples {
    [examples.Example(name: _, code: code), ..] -> code
    [] -> ""
  }
  #(
    Model(
      source: code,
      output: "",
      errors: None,
      worker: None,
      output_panel: Closed,
      popup: None,
    ),
    effect.none(),
  )
}

// Todo
// type InterpreterStopped {
//   Ok
//   Errors(List(String))
//   Aborted
// }

pub type Msg {
  UserClickedRun
  UserClickedStop(bindings.WebWorker)
  UserClearedOutput
  WorkerStarted(Result(bindings.WebWorker, Nil))
  WorkerStopped
  InterpreterSentOutput(String)
  UserEditedSource(String)
  InterpreterFinished(Result(Nil, List(String)))
  UserClosedOutput
  UserMinimizedOutput
  UserMaximizedOutput
  UserOpenedExamples
  UserSelectedExample(String)
  UserOpenedAbout
  UserClosedPopup
}

fn interpret(source: String) -> effect.Effect(Msg) {
  effect.from(fn(dispatch) {
    let output_callback = fn(message) {
      dispatch(InterpreterSentOutput(message))
    }
    let result_callback = fn(errors) {
      let errors = array.to_list(errors)
      case errors {
        [] -> dispatch(InterpreterFinished(Ok(Nil)))
        _ -> dispatch(InterpreterFinished(Error(errors)))
      }
      dispatch(WorkerStopped)
    }
    let result = bindings.start_worker(source, output_callback, result_callback)
    dispatch(WorkerStarted(result))
    Nil
  })
}

fn stop_worker(worker: bindings.WebWorker) -> effect.Effect(Msg) {
  effect.from(fn(dispatch) {
    bindings.stop_worker(worker)
    dispatch(WorkerStopped)
  })
}

pub fn update(model: Model, msg: Msg) -> #(Model, effect.Effect(Msg)) {
  case msg {
    WorkerStarted(Ok(worker)) -> #(
      Model(..model, worker: Some(worker)),
      effect.none(),
    )
    // Todo display error
    WorkerStarted(Error(_)) -> #(model, effect.none())
    WorkerStopped -> #(Model(..model, worker: None), effect.none())
    UserClickedStop(worker) -> #(model, stop_worker(worker))
    UserEditedSource(source) -> #(Model(..model, source: source), effect.none())
    UserClearedOutput -> #(
      Model(..model, output: "", errors: None),
      effect.none(),
    )
    UserClickedRun -> #(
      Model(
        ..model,
        output: "",
        errors: None,
        output_panel: case model.output_panel {
          Closed -> Minimized
          _ -> model.output_panel
        },
      ),
      interpret(model.source),
    )
    InterpreterSentOutput(message) -> #(
      Model(..model, output: model.output <> message),
      effect.none(),
    )
    InterpreterFinished(Ok(_)) -> #(model, effect.none())
    InterpreterFinished(Error(errors)) -> #(
      Model(..model, errors: Some(errors)),
      effect.none(),
    )
    UserClosedOutput -> #(Model(..model, output_panel: Closed), effect.none())
    UserMaximizedOutput -> #(
      Model(..model, output_panel: Maximized),
      effect.none(),
    )
    UserMinimizedOutput -> #(
      Model(..model, output_panel: Minimized),
      effect.none(),
    )
    UserOpenedExamples -> #(
      Model(..model, popup: Some(Examples)),
      effect.none(),
    )
    UserSelectedExample(code) -> #(
      Model(..model, source: code, popup: None),
      effect.none(),
    )
    UserOpenedAbout -> #(Model(..model, popup: Some(About)), effect.none())
    UserClosedPopup -> #(Model(..model, popup: None), effect.none())
  }
}

fn icon_button(
  icon icon: element.Element(a),
  text text: String,
  attributes attributes: List(attribute.Attribute(a)),
) -> element.Element(a) {
  html.button(
    [
      attribute.class(
        "bg-emerald-500 hover:bg-emerald-400 active:bg-emerald-600 active:shadow-none rounded-full px-3 py-1 shadow-md font-semibold flex flex-row items-center space-x-2",
      ),
      ..attributes
    ],
    [icon, html.div([], [element.text(text)])],
  )
}

fn header(model: Model) -> element.Element(Msg) {
  html.header(
    [
      attribute.class(
        "bg-emerald-800 text-slate-50 shrink-0 flex flex-row items-center p-4 overflow-x-auto",
      ),
    ],
    [
      html.h1(
        [
          attribute.class(
            "text-2xl font-bold whitespace-nowrap hidden sm:block pr-8",
          ),
        ],
        [html.text("LLL Playground")],
      ),
      html.div([attribute.class("flex flex-row space-x-4")], [
        case model.worker {
          None ->
            icon_button(icon: icons.play_fill([]), text: "Run", attributes: [
              event.on_click(UserClickedRun),
            ])
          Some(worker) ->
            icon_button(icon: icons.stop_fill([]), text: "Stop", attributes: [
              event.on_click(UserClickedStop(worker)),
            ])
        },
        icon_button(icon: icons.files_bold([]), text: "Examples", attributes: [
          event.on_click(UserOpenedExamples),
        ]),
        icon_button(icon: icons.info_bold([]), text: "About", attributes: [
          event.on_click(UserOpenedAbout),
        ]),
      ]),
    ],
  )
}

fn output_panel_header_buttons(model: Model) -> element.Element(Msg) {
  let fullscreen = [
    html.button(
      [attribute.title("Minimize"), event.on_click(UserMinimizedOutput)],
      [icons.caret_down_bold([])],
    ),
    html.button([attribute.title("Close"), event.on_click(UserClosedOutput)], [
      icons.caret_double_down_bold([]),
    ]),
  ]
  let open = [
    html.button([attribute.title("Close"), event.on_click(UserClosedOutput)], [
      icons.caret_down_bold([]),
    ]),
    html.button(
      [attribute.title("Maximize"), event.on_click(UserMaximizedOutput)],
      [icons.caret_up_bold([])],
    ),
  ]
  let closed = [
    html.button([attribute.title("Open"), event.on_click(UserMinimizedOutput)], [
      icons.caret_up_bold([]),
    ]),
    html.button(
      [attribute.title("Maximize"), event.on_click(UserMaximizedOutput)],
      [icons.caret_double_up_bold([])],
    ),
  ]

  html.div([attribute.class("space-x-4")], case model.output_panel {
    Maximized -> fullscreen
    Minimized -> open
    Closed -> closed
  })
}

fn error_box(error: String) -> element.Element(Msg) {
  // Syntax Error [Line x, Col y]
  // Error text
  html.div(
    [attribute.class("px-4 py-2 mt-2 text-rose-950 bg-rose-600 bg-opacity-80")],
    [html.text(error)],
  )
}

fn output_panel(model: Model) -> element.Element(Msg) {
  let output_hidden = case model.output_panel {
    Closed -> " hidden"
    _ -> ""
  }
  let style = case model.output_panel {
    Maximized -> [#("flex-grow", "1")]
    Minimized -> [#("height", "30vh")]
    Closed -> []
  }
  html.div(
    [attribute.class("relative flex flex-col min-h-0"), attribute.style(style)],
    [
      html.header(
        [
          attribute.class(
            "bg-emerald-600 text-slate-50 shrink-0 h-10 flex items-center justify-between px-4",
          ),
        ],
        [
          html.h2([attribute.class("font-semibold")], [html.text("Output")]),
          output_panel_header_buttons(model),
        ],
      ),
      html.div(
        [
          attribute.class("bg-emerald-50 flex-grow overflow-y-auto"),
          attribute.class(output_hidden),
        ],
        [
          html.div(
            [
              attribute.class(
                "max-h-full overflow-y-auto flex flex-col-reverse p-2",
              ),
            ],
            [
              html.div([], case model.errors {
                None -> []
                Some(errors) -> {
                  list.map(errors, error_box)
                }
              }),
              html.div(
                [attribute.class("whitespace-pre font-mono")],
                model.output
                  |> string.split("\n")
                  |> list.map(fn(line) { html.text(line) })
                  |> list.intersperse(html.br([])),
              ),
            ],
          ),
          html.button(
            [
              attribute.title("Clear output"),
              attribute.class(
                "absolute right-4 bottom-4 rounded-full w-10 h-10 opacity-80 text-slate-50 bg-emerald-400 hover:bg-emerald-500 active:bg-emerald-500 active:opacity-100 inline-flex items-center justify-center",
              ),
              event.on_click(UserClearedOutput),
            ],
            [icons.trash_fill([attribute.class("w-5 h-5")])],
          ),
        ],
      ),
    ],
  )
}

fn editor(model: Model) -> element.Element(Msg) {
  let hidden = case model.output_panel {
    Maximized -> " hidden"
    _ -> ""
  }
  html.textarea(
    [
      attribute.class(
        "bg-slate-50 font-mono border-none resize-none flex-grow w-full p-2",
      ),
      attribute.class(hidden),
      attribute.attribute("spellcheck", "false"),
      attribute.style([
        #("-webkit-box-shadow", "none"),
        #("-moz-box-shadow", "none"),
        #("box-shadow", "none"),
      ]),
      event.on_input(UserEditedSource),
    ],
    model.source,
  )
}

fn example_window() -> element.Element(Msg) {
  html.div(
    [attribute.class("grow overflow-scroll max-h-96 flex flex-col")],
    examples.examples
      |> list.index_map(fn(example, n) {
        html.button(
          [
            attribute.class(
              "text-left px-4 py-2 hover:bg-slate-300 hover:bg-opacity-50 active:bg-slate-400 active:bg-opacity-50 w-full",
            ),
            event.on_click(UserSelectedExample(example.code)),
          ],
          [html.text(int.to_string(n + 1) <> ". " <> example.name)],
        )
      }),
  )
}

fn about_window() -> element.Element(Msg) {
  html.div([attribute.class("p-4 space-y-3")], [
    html.div([], [
      html.text(
        "This is an interactive playground for my toy language LLL. Check out the examples to see some of the things it is capable of.",
      ),
    ]),
    html.hr([]),
    html.div([], [
      html.h2([attribute.class("font-semibold")], [html.text("Links")]),
      html.ul(
        [],
        [
          #("Playground source", "https://gitlab.com/leah_u/lll_playground"),
          #("Language source", "https://gitlab.com/leah_u/lll"),
          #("My website", "https://ulmschneider.ch"),
        ]
          |> list.map(fn(elem) {
            html.li([attribute.class("pt-2")], [
              html.text(elem.0 <> ":"),
              html.br([]),
              html.a(
                [
                  attribute.class("text-indigo-700 underline"),
                  attribute.href(elem.1),
                  attribute.target("_blank"),
                ],
                [html.text(elem.1)],
              ),
            ])
          }),
      ),
    ]),
    html.hr([]),
    html.div([], [html.text("Created by Leah Ulmschneider")]),
  ])
}

fn popup(
  icon: element.Element(Msg),
  title: String,
  content: element.Element(Msg),
  visible: Bool,
) -> element.Element(Msg) {
  use <- bool.guard(!visible, html.div([], []))

  html.div(
    [
      attribute.class(
        "fixed top-0 left-0 w-full h-full p-8 flex flex-col justify-center items-center bg-gray-900 bg-opacity-40",
      ),
      event.on_click(UserClosedPopup),
    ],
    [
      html.div(
        [
          attribute.class("bg-slate-50 w-full max-w-96 shadow-xl"),
          event.on("click", fn(event) {
            event.stop_propagation(event)
            Error([])
          }),
        ],
        [
          html.header(
            [
              attribute.class(
                "overflow-scroll bg-emerald-600 text-slate-50 flex flex-row justify-between items-center h-10 px-4 space-x-10",
              ),
            ],
            [
              html.div(
                [attribute.class("flex flex-row space-x-2 items-center")],
                [
                  icon,
                  html.h1([attribute.class("font-semibold")], [html.text(title)]),
                ],
              ),
              html.button(
                [attribute.title("Close"), event.on_click(UserClosedPopup)],
                [icons.x_bold([])],
              ),
            ],
          ),
          content,
        ],
      ),
    ],
  )
}

pub fn view(model: Model) -> element.Element(Msg) {
  html.div(
    [
      attribute.class("flex flex-col h-screen"),
      attribute.style([#("height", "100dvh")]),
    ],
    [
      header(model),
      editor(model),
      output_panel(model),
      popup(
        icons.files_bold([]),
        "Examples",
        example_window(),
        model.popup == Some(Examples),
      ),
      popup(
        icons.info_bold([]),
        "About",
        about_window(),
        model.popup == Some(About),
      ),
    ],
  )
}
