import gleam/javascript/array.{type Array}

pub type WebWorker

@external(javascript, "./lll_playground_ffi.mjs", "start_worker")
pub fn start_worker(
  source: String,
  output_callback: fn(String) -> Nil,
  result_callback: fn(Array(String)) -> Nil,
) -> Result(WebWorker, Nil)

@external(javascript, "./lll_playground_ffi.mjs", "stop_worker")
pub fn stop_worker(worker: WebWorker) -> Nil
