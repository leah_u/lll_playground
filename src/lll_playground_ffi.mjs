import { Ok, Error } from './gleam.mjs'

export function start_worker(source, output_callback, result_callback) {
    if (!window.Worker) {
        console.log('Your browser doesn\'t support web workers.');
        return new Error(null);
    }
    const worker = new Worker('/worker.js');
    worker.onmessage = (msg) => {
        if (typeof (msg.data) === 'string') {
            output_callback(msg.data);
        } else if (msg.data.constructor === Array) {
            result_callback(msg.data);
        }
    }
    worker.postMessage(source);
    return new Ok(worker);
}

export function stop_worker(worker) {
    worker.terminate();
}
