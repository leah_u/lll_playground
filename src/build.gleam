import argv
import esgleam
import gleam/io
import gleamyshell
import glint
import lustre/attribute
import lustre/element
import lustre/element/html
import simplifile
import snag
import tailwind

fn outdir_flag() -> glint.Flag(String) {
  glint.string_flag("outdir")
  |> glint.flag_default("dist")
  |> glint.flag_help("Set the output directory for the web app")
  |> glint.flag_constraint(fn(dir) {
    case dir {
      "" -> snag.error("cannot be empty")
      _ -> Ok(dir)
    }
  })
}

pub fn main() {
  glint.new()
  |> glint.with_name("build")
  |> glint.as_module
  |> glint.pretty_help(glint.default_pretty_help())
  |> glint.add(at: [], do: cmd_all())
  |> glint.add(at: ["rust"], do: cmd_rust())
  |> glint.add(at: ["gleam"], do: cmd_gleam())
  |> glint.run(argv.load().arguments)
}

fn cmd_all() {
  use outdir_arg <- glint.flag(outdir_flag())
  use _named, _unnamed, flags <- glint.command()

  let assert Ok(outdir) = outdir_arg(flags)

  build_rust()
  build_gleam(outdir)
}

fn cmd_rust() {
  use _named, _unnamed, _flags <- glint.command()
  build_rust()
}

fn build_rust() {
  let assert Ok(gleamyshell.CommandOutput(0, _)) =
    gleamyshell.execute(
      "cargo",
      args: ["build", "--target=wasm32-unknown-unknown", "--release"],
      in: "rust",
    )

  let assert Ok(gleamyshell.CommandOutput(0, _)) =
    gleamyshell.execute(
      "wasm-bindgen",
      args: [
        "--out-dir=dist", "--target=web", "--omit-default-module-path",
        "--no-typescript",
        "target/wasm32-unknown-unknown/release/lll_playground.wasm",
      ],
      in: "rust",
    )

  Nil
}

fn cmd_gleam() {
  use outdir_arg <- glint.flag(outdir_flag())
  use _named, _unnamed, flags <- glint.command()

  let assert Ok(outdir) = outdir_arg(flags)
  build_gleam(outdir)
}

fn build_gleam(outdir: String) {
  // Build main application
  let assert Ok(_) =
    esgleam.new(outdir)
    |> esgleam.entry("lll_playground.gleam")
    |> esgleam.kind(esgleam.Script)
    |> esgleam.minify(True)
    |> esgleam.bundle

  // Copy wasm files to build directory
  let assert Ok(gleamyshell.CommandOutput(0, _)) =
    gleamyshell.execute(
      "cp",
      args: [
        "rust/dist/lll_playground.js", "rust/dist/lll_playground_bg.wasm",
        "build/dev/javascript/lll_playground",
      ],
      in: ".",
    )

  // Build web worker
  let assert Ok(_) =
    esgleam.new(outdir)
    |> esgleam.entry("worker.js")
    |> esgleam.raw("--loader:.wasm=dataurl")
    |> esgleam.minify(True)
    |> esgleam.bundle

  // Build index.html
  let assert Ok(_) =
    index_html()
    |> simplifile.write(to: outdir <> "/index.html")

  // Build tailwind
  let assert Ok(_) =
    [
      "--config=tailwind.config.js",
      // "--input=./src/css/app.css",
      "--output=" <> outdir <> "/styles.css",
      "--minify",
    ]
    |> tailwind.install_and_run

  io.println("\n✅ Finished building")

  Nil
}

fn index_html() -> String {
  html.html([attribute.attribute("lang", "en")], [
    html.head([], [
      html.meta([attribute.attribute("charset", "UTF-8")]),
      html.meta([
        attribute.attribute("content", "width=device-width, initial-scale=1.0"),
        attribute.name("viewport"),
      ]),
      html.title([], "LLL Playground"),
      html.link([attribute.href("/styles.css"), attribute.rel("stylesheet")]),
      html.script(
        [attribute.src("/lll_playground.mjs"), attribute.type_("module")],
        "",
      ),
      html.link([
        attribute.href("https://fonts.googleapis.com"),
        attribute.rel("preconnect"),
      ]),
      html.link([
        attribute.attribute("crossorigin", ""),
        attribute.href("https://fonts.gstatic.com"),
        attribute.rel("preconnect"),
      ]),
      html.link([
        attribute.rel("stylesheet"),
        attribute.href(
          "https://fonts.googleapis.com/css2?family=Roboto+Mono&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap",
        ),
      ]),
      html.link([
        attribute.rel("preload"),
        attribute.attribute("as", "script"),
        attribute.href("/worker.js"),
      ]),
    ]),
    html.body([], [html.div([attribute.id("app")], [])]),
  ])
  |> element.to_document_string
}
