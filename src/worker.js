import init, { interpret } from './lll_playground.js';
import wasmData from './lll_playground_bg.wasm';

async function main() {
    const wasm_promise = init(wasmData)

    addEventListener("message", async (msg) => {
        await wasm_promise;
        const result = interpret(msg.data, postMessage);
        postMessage(result);
        close();
    });
}
main();
