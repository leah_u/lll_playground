pub type Example {
  Example(name: String, code: String)
}

pub const examples: List(Example) = [
  Example(
    name: "Hello World",
    code: "func main() {
    print(\"Hello World!\");
}",
  ),
  Example(
    name: "FizzBuzz",
    code: "let iterations = 100;

func main() {
    for var n = 1; n < iterations + 1; n += 1 {
        if n % 15 == 0 {
            print(\"fizzbuzz\");
        } else if n % 3 == 0 {
            print(\"fizz\");
        } else if n % 5 == 0 {
            print(\"buzz\");
        } else {
            print(n);
        }
    }
}",
  ),
  Example(
    name: "Fibonacci",
    code: "func fibonacci(n) {
    if n < 2 {
        1
    } else {
        fibonacci(n - 1) + fibonacci(n - 2)
    }
}

func main() {
    var n = 0;
    while n < 15 {
        print(fibonacci(n));
        n += 1;
    }
}",
  ),
  Example(
    name: "Rule 110",
    code: "func getNewState(pattern) {
    let states = [0, 1, 1, 1, 0, 1, 1, 0];
    states[pattern]
}

func iterate(state) {
    let length = len(state);
    let newState = [getNewState(state[1] + state[0] * 2)];
    for var i = 1; i < length - 1; i += 1 {
        let pattern = state[i+1] + state[i] * 2 + state[i-1] * 4;
        newState[i] = getNewState(pattern);
    }
    newState[length - 1] = getNewState(state[length - 1] * 2 + state[length - 2] * 4);
    newState
}

func printState(state) {
    var output = \"\";
    for var i = 0; i < len(state); i += 1 {
        output += if state[i] == 0 {\" \"} else {\"#\"};
    }
    print(output);
}

func main() {
    let size = 50;
    var state = [];
    for var i = 0; i < size - 1; i += 1 {
        state[i] = 0;
    }
    state[size - 1] = 1;
    printState(state);
    for var n = 0; n < len(state) - 1; n += 1 {
        state = iterate(state);
        printState(state);
    }
}",
  ),
]
