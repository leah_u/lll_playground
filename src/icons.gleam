import gleam/list
import lustre/attribute.{type Attribute} as attr
import lustre/element.{type Element}
import lustre/element/svg

// Copied from https://github.com/brettkolodny/phosphor-lustre

pub fn play_fill(attrs: List(Attribute(msg))) -> Element(msg) {
  let base_attributes = [
    attr.attribute("xmlns", "http://www.w3.org/2000/svg"),
    attr.attribute("fill", "currentColor"),
    attr.attribute("stroke", "currentColor"),
    attr.attribute("stroke-linecap", "round"),
    attr.attribute("viewBox", "0 0 256 256"),
    attr.attribute("width", "1em"),
    attr.attribute("height", "1em"),
    ..attrs
  ]

  let combined_attributes = list.concat([base_attributes, attrs])

  svg.svg(combined_attributes, [
    svg.path([
      attr.attribute(
        "d",
        "M240,128a15.74,15.74,0,0,1-7.6,13.51L88.32,229.65a16,16,0,0,1-16.2.3A15.86,15.86,0,0,1,64,216.13V39.87a15.86,15.86,0,0,1,8.12-13.82,16,16,0,0,1,16.2.3L232.4,114.49A15.74,15.74,0,0,1,240,128Z",
      ),
    ]),
  ])
}

pub fn stop_fill(attrs: List(Attribute(msg))) -> Element(msg) {
  let base_attributes = [
    attr.attribute("xmlns", "http://www.w3.org/2000/svg"),
    attr.attribute("fill", "currentColor"),
    attr.attribute("stroke", "currentColor"),
    attr.attribute("stroke-linecap", "round"),
    attr.attribute("viewBox", "0 0 256 256"),
    attr.attribute("width", "1em"),
    attr.attribute("height", "1em"),
    ..attrs
  ]

  let combined_attributes = list.concat([base_attributes, attrs])

  svg.svg(combined_attributes, [
    svg.path([
      attr.attribute(
        "d",
        "M216,56V200a16,16,0,0,1-16,16H56a16,16,0,0,1-16-16V56A16,16,0,0,1,56,40H200A16,16,0,0,1,216,56Z",
      ),
    ]),
  ])
}

pub fn caret_up_bold(attrs: List(Attribute(msg))) -> Element(msg) {
  let base_attributes = [
    attr.attribute("xmlns", "http://www.w3.org/2000/svg"),
    attr.attribute("fill", "currentColor"),
    attr.attribute("stroke", "currentColor"),
    attr.attribute("stroke-linecap", "round"),
    attr.attribute("viewBox", "0 0 256 256"),
    attr.attribute("width", "1em"),
    attr.attribute("height", "1em"),
    ..attrs
  ]

  let combined_attributes = list.concat([base_attributes, attrs])

  svg.svg(combined_attributes, [
    svg.path([
      attr.attribute(
        "d",
        "M216.49,168.49a12,12,0,0,1-17,0L128,97,56.49,168.49a12,12,0,0,1-17-17l80-80a12,12,0,0,1,17,0l80,80A12,12,0,0,1,216.49,168.49Z",
      ),
    ]),
  ])
}

pub fn caret_down_bold(attrs: List(Attribute(msg))) -> Element(msg) {
  let base_attributes = [
    attr.attribute("xmlns", "http://www.w3.org/2000/svg"),
    attr.attribute("fill", "currentColor"),
    attr.attribute("stroke", "currentColor"),
    attr.attribute("stroke-linecap", "round"),
    attr.attribute("viewBox", "0 0 256 256"),
    attr.attribute("width", "1em"),
    attr.attribute("height", "1em"),
    ..attrs
  ]

  let combined_attributes = list.concat([base_attributes, attrs])

  svg.svg(combined_attributes, [
    svg.path([
      attr.attribute(
        "d",
        "M216.49,104.49l-80,80a12,12,0,0,1-17,0l-80-80a12,12,0,0,1,17-17L128,159l71.51-71.52a12,12,0,0,1,17,17Z",
      ),
    ]),
  ])
}

pub fn caret_double_up_bold(attrs: List(Attribute(msg))) -> Element(msg) {
  let base_attributes = [
    attr.attribute("xmlns", "http://www.w3.org/2000/svg"),
    attr.attribute("fill", "currentColor"),
    attr.attribute("stroke", "currentColor"),
    attr.attribute("stroke-linecap", "round"),
    attr.attribute("viewBox", "0 0 256 256"),
    attr.attribute("width", "1em"),
    attr.attribute("height", "1em"),
    ..attrs
  ]

  let combined_attributes = list.concat([base_attributes, attrs])

  svg.svg(combined_attributes, [
    svg.path([
      attr.attribute(
        "d",
        "M216.49,191.51a12,12,0,0,1-17,17L128,137,56.49,208.49a12,12,0,0,1-17-17l80-80a12,12,0,0,1,17,0Zm-160-63L128,57l71.51,71.52a12,12,0,0,0,17-17l-80-80a12,12,0,0,0-17,0l-80,80a12,12,0,0,0,17,17Z",
      ),
    ]),
  ])
}

pub fn caret_double_down_bold(attrs: List(Attribute(msg))) -> Element(msg) {
  let base_attributes = [
    attr.attribute("xmlns", "http://www.w3.org/2000/svg"),
    attr.attribute("fill", "currentColor"),
    attr.attribute("stroke", "currentColor"),
    attr.attribute("stroke-linecap", "round"),
    attr.attribute("viewBox", "0 0 256 256"),
    attr.attribute("width", "1em"),
    attr.attribute("height", "1em"),
    ..attrs
  ]

  let combined_attributes = list.concat([base_attributes, attrs])

  svg.svg(combined_attributes, [
    svg.path([
      attr.attribute(
        "d",
        "M216.49,127.51a12,12,0,0,1,0,17l-80,80a12,12,0,0,1-17,0l-80-80a12,12,0,1,1,17-17L128,199l71.51-71.52A12,12,0,0,1,216.49,127.51Zm-97,17a12,12,0,0,0,17,0l80-80a12,12,0,0,0-17-17L128,119,56.49,47.51a12,12,0,0,0-17,17Z",
      ),
    ]),
  ])
}

pub fn trash_fill(attrs: List(Attribute(msg))) -> Element(msg) {
  let base_attributes = [
    attr.attribute("xmlns", "http://www.w3.org/2000/svg"),
    attr.attribute("fill", "currentColor"),
    attr.attribute("stroke", "currentColor"),
    attr.attribute("stroke-linecap", "round"),
    attr.attribute("viewBox", "0 0 256 256"),
    attr.attribute("width", "1em"),
    attr.attribute("height", "1em"),
    ..attrs
  ]

  let combined_attributes = list.concat([base_attributes, attrs])

  svg.svg(combined_attributes, [
    svg.path([
      attr.attribute(
        "d",
        "M216,48H176V40a24,24,0,0,0-24-24H104A24,24,0,0,0,80,40v8H40a8,8,0,0,0,0,16h8V208a16,16,0,0,0,16,16H192a16,16,0,0,0,16-16V64h8a8,8,0,0,0,0-16ZM112,168a8,8,0,0,1-16,0V104a8,8,0,0,1,16,0Zm48,0a8,8,0,0,1-16,0V104a8,8,0,0,1,16,0Zm0-120H96V40a8,8,0,0,1,8-8h48a8,8,0,0,1,8,8Z",
      ),
    ]),
  ])
}

pub fn info_bold(attrs: List(Attribute(msg))) -> Element(msg) {
  let base_attributes = [
    attr.attribute("xmlns", "http://www.w3.org/2000/svg"),
    attr.attribute("fill", "currentColor"),
    attr.attribute("stroke", "currentColor"),
    attr.attribute("stroke-linecap", "round"),
    attr.attribute("viewBox", "0 0 256 256"),
    attr.attribute("width", "1em"),
    attr.attribute("height", "1em"),
    ..attrs
  ]

  let combined_attributes = list.concat([base_attributes, attrs])

  svg.svg(combined_attributes, [
    svg.path([
      attr.attribute(
        "d",
        "M108,84a16,16,0,1,1,16,16A16,16,0,0,1,108,84Zm128,44A108,108,0,1,1,128,20,108.12,108.12,0,0,1,236,128Zm-24,0a84,84,0,1,0-84,84A84.09,84.09,0,0,0,212,128Zm-72,36.68V132a20,20,0,0,0-20-20,12,12,0,0,0-4,23.32V168a20,20,0,0,0,20,20,12,12,0,0,0,4-23.32Z",
      ),
    ]),
  ])
}

pub fn files_bold(attrs: List(Attribute(msg))) -> Element(msg) {
  let base_attributes = [
    attr.attribute("xmlns", "http://www.w3.org/2000/svg"),
    attr.attribute("fill", "currentColor"),
    attr.attribute("stroke", "currentColor"),
    attr.attribute("stroke-linecap", "round"),
    attr.attribute("viewBox", "0 0 256 256"),
    attr.attribute("width", "1em"),
    attr.attribute("height", "1em"),
    ..attrs
  ]

  let combined_attributes = list.concat([base_attributes, attrs])

  svg.svg(combined_attributes, [
    svg.path([
      attr.attribute(
        "d",
        "M220.49,59.51l-40-40A12,12,0,0,0,172,16H92A20,20,0,0,0,72,36V56H56A20,20,0,0,0,36,76V216a20,20,0,0,0,20,20H164a20,20,0,0,0,20-20V196h20a20,20,0,0,0,20-20V68A12,12,0,0,0,220.49,59.51ZM160,212H60V80h67l33,33Zm40-40H184V108a12,12,0,0,0-3.51-8.49l-40-40A12,12,0,0,0,132,56H96V40h71l33,33Zm-56-28a12,12,0,0,1-12,12H88a12,12,0,0,1,0-24h44A12,12,0,0,1,144,144Zm0,40a12,12,0,0,1-12,12H88a12,12,0,0,1,0-24h44A12,12,0,0,1,144,184Z",
      ),
    ]),
  ])
}

pub fn x_bold(attrs: List(Attribute(msg))) -> Element(msg) {
  let base_attributes = [
    attr.attribute("xmlns", "http://www.w3.org/2000/svg"),
    attr.attribute("fill", "currentColor"),
    attr.attribute("stroke", "currentColor"),
    attr.attribute("stroke-linecap", "round"),
    attr.attribute("viewBox", "0 0 256 256"),
    attr.attribute("width", "1em"),
    attr.attribute("height", "1em"),
    ..attrs
  ]

  let combined_attributes = list.concat([base_attributes, attrs])

  svg.svg(combined_attributes, [
    svg.path([
      attr.attribute(
        "d",
        "M208.49,191.51a12,12,0,0,1-17,17L128,145,64.49,208.49a12,12,0,0,1-17-17L111,128,47.51,64.49a12,12,0,0,1,17-17L128,111l63.51-63.52a12,12,0,0,1,17,17L145,128Z",
      ),
    ]),
  ])
}
