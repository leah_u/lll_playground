mod utils;

use core::str;

use utils::set_panic_hook;
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);
}

struct OutputWriter<'a> {
    callback: &'a js_sys::Function,
}

impl <'a> OutputWriter<'a> {
    fn new(callback: &'a js_sys::Function) -> Self {
        Self{callback}
    }
}

impl<'a> std::io::Write for OutputWriter<'a> {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        let message = str::from_utf8(buf)
            .map_err(|err| std::io::Error::new(std::io::ErrorKind::InvalidData, err))?;

        self.callback
            .call1(&JsValue::null(), &JsValue::from_str(message))
            .map_err(|err| {
                std::io::Error::new(
                    std::io::ErrorKind::Other,
                    err.as_string().unwrap_or("Error calling output callback".to_owned()),
                )
            })?;
        Ok(buf.len())
    }

    fn flush(&mut self) -> std::io::Result<()> {
        Ok(())
    }
}

#[wasm_bindgen]
pub fn interpret(source: String, print_callback: &js_sys::Function) -> Vec<String> {
    set_panic_hook();

    let mut output_writer = OutputWriter::new(print_callback);
    match lll::run("", &source, &mut output_writer) {
        Ok(_) => vec![],
        Err(errors) => errors.into_iter().map(|err| format!("{err}")).collect(),
    }
}

#[wasm_bindgen]
pub fn greet() -> String {
    "Hello from Rust!".to_owned()
}
