# LLL Playground

## Building

``` shell
gleam run -m build
```

`gleam run -m build rust` only builds the interpreter (written in Rust) and
places it in `rust/dist`.

`gleam run -m build gleam` takes the compiled interpreter and builds the final
Gleam app.

Running `gleam run -m build` runs both build steps.
